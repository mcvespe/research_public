import numpy as np
from scipy import stats
import sys
import csv
# Our files
import abc_smc
import csmm
# csmm will require Greg Ver Steeg's NPEET toolbox; see http://www.isi.edu/~gregv/npeet.html

### Command line arguments:
# python unknown_tau.py seed_start n_sims num_particles_use time_steps_use
# e.g. python unknown_tau.py 51897779 100 100 8
seed_start = int(sys.argv[1])
n_sims = int(sys.argv[2])
num_particles_use = int(sys.argv[3])
time_steps_use = int(sys.argv[4])

# Compute the ewcdf from a weighted sample
def ewcdf(sample_arg, weights_arg):
    def to_return(x_arg):
        return np.sum(weights_arg[x_arg >= sample_arg])
    return to_return

# Compute posterior error
def posterior_error(posterior_object, smc_object, lattice_points):
    tmp_particles = np.array(smc_object['particles'].T).reshape(smc_object['particles'].shape[0])
    tmp_weights = smc_object['weights']
    tmp_ewcdf = ewcdf(tmp_particles, tmp_weights)
    
    smc_ewcdf = np.zeros(lattice_points.shape[0])
    for ii in range(lattice_points.shape[0]):
        smc_ewcdf[ii] = tmp_ewcdf(lattice_points[ii])
    eval_posterior = posterior_object.cdf(lattice_points)
    return np.mean((smc_ewcdf - eval_posterior)**(2.0))

## Actually do stuff for this simulation study
# Define the prior
alpha_prior = 1.0
beta_prior = 1.0
gamma_prior = stats.gamma(a = alpha_prior, scale = 1.0/beta_prior)
lattice_points_use = np.linspace(gamma_prior.ppf(0.001),gamma_prior.ppf(0.999), num = 1000)

# Define the model
n_data = 32
mu_true = 0
def model_draw_normal(tau_arg):
    return np.random.normal(loc = mu_true, scale = 1.0/np.sqrt(tau_arg), size = n_data)

# true parameter value
tau_true = 0.25

# Specify different summary statistics
def summarize_mss(x_arg):
    return np.sum(x_arg**(2.0))
def summarize_mean(x_arg):
    return np.mean(x_arg)
def summarize_IQR(x_arg):
    return (np.percentile(x_arg, 75) - np.percentile(x_arg, 25))

## Learn a mapping using the CSMM!!
# Build up training set
# Initialize the random seed
np.random.seed(seed_start)
dims_keep = 0
n_train = 200
tau_train = gamma_prior.rvs(n_train)
tau_train_mat = np.matrix(tau_train).T
theta_train_mat = np.sqrt(tau_train_mat)
x_train = np.zeros((n_train,n_data))
for train_iter in range(len(tau_train)):
    x_train[train_iter,] = model_draw_normal(tau_train[train_iter])

# Try a bunch of values of \lambda and pick the one that optimizes the information criterion
log2_lambda_range = [-8.0,8.0]
lambda_try = 2.0**np.linspace(log2_lambda_range[0],log2_lambda_range[1],np.ptp(log2_lambda_range) + 1)
store_appsuff = np.zeros(len(lambda_try))
store_appsuff_pairwise = np.zeros(len(lambda_try))
for lambda_iter in range(len(lambda_try)):
    csmm_object = csmm.csmm(theta_train = theta_train_mat, x_train = x_train, lambda_aff = lambda_try[lambda_iter])
    store_appsuff[lambda_iter] = csmm_object.approx_suff(dims_keep = dims_keep, pairwise_mi = False)
    store_appsuff_pairwise[lambda_iter] = csmm_object.approx_suff(dims_keep = dims_keep, pairwise_mi = True)
    print (lambda_iter,lambda_try[lambda_iter], store_appsuff[lambda_iter])

to_write = np.vstack([lambda_try,store_appsuff,store_appsuff_pairwise]).T
results_file = open('diagnostic_results_tau.csv','wb')
tmp_writer = csv.writer(results_file)
tmp_writer.writerows(to_write)
results_file.close()

lambda_best = lambda_try[np.argmax(store_appsuff)]
print('Using lambda = ' + str(lambda_best))

csmm_best = csmm.csmm(theta_train = theta_train_mat, x_train = x_train, lambda_aff = lambda_best)
def summarize_csmm(x_arg):
    return csmm_best.nystrom(np.matrix(x_arg), dims_keep = dims_keep)

summary_function_list = [summarize_mss, summarize_csmm, summarize_IQR, summarize_mean]
n_summary_functions = len(summary_function_list)
store_errors = np.zeros([n_sims,n_summary_functions])

for sim_iter in range(n_sims):
    print "Running simulation #" + str(sim_iter)
    np.random.seed(seed_start + sim_iter + 1)
    x_obs = model_draw_normal(tau_true)

    obs_file = open('x_obs.csv','wb')
    tmp_writer = csv.writer(obs_file)
    tmp_writer.writerows(np.array(np.matrix(x_obs).T))
    obs_file.close()
    
    # The posterior depends on the observed data...
    alpha_posterior = alpha_prior + n_data/2.0
    beta_posterior = beta_prior + summarize_mss(x_obs)/2.0
    gamma_posterior = stats.gamma(a = alpha_posterior, scale = 1.0/beta_posterior)

    for func_iter in range(n_summary_functions):
        this_func = summary_function_list[func_iter]
        this_smc_out = abc_smc.abc_smc(summary_func = this_func, obs_data = x_obs,prior_object = gamma_prior, model_draw_func = model_draw_normal, num_particles = num_particles_use, time_steps = time_steps_use, perturbation_bw_start = 0.25, epsilon_start = 5)
        store_errors[sim_iter,func_iter] = posterior_error(posterior_object = gamma_posterior, smc_object = this_smc_out, lattice_points = lattice_points_use)

    results_file = open('cdf_results.csv','wb')
    tmp_writer = csv.writer(results_file)
    tmp_writer.writerows(store_errors)
    results_file.close()