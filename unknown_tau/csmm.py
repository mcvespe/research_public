import numpy as np
from scipy import spatial
from scipy import linalg as LA
from matplotlib import pyplot as plt
import entropy_estimators as ee


class csmm:
    """A class that implements the common space mapping method (Vespe et al. 201?)."""
    
    def __init__(self, theta_train, x_train, lambda_aff, bw_quantile = 0.05, kernel_order = 1.0, to_sort = True):
        """Construct an instance of the common space mapping.
            
        Keyword arguments:
        theta_train -- component of the simulated training set corresponding to parameter values
        x_train -- component of the simulated training set corresponding to data values
        lambda_aff -- the affinity tuning parameter -- how heavily to weight relationships between pairs of corresponding parameters and data
        bw_quantile -- the quantile of the intra-parameter and intra-data distances to use as a bandwidth for K(.,.) (default 0.05)
        kernel_order -- 1 for exponential kernel, 2 for Gaussian kernel (default 1)
        to_sort -- bool, are the data IID? if so, use order statistics instead of raw data (default yes)
        """
        self._theta_train = theta_train
        self._x_train = x_train
        self._lambda_aff = lambda_aff
        self._bw_quantile = bw_quantile
        self._kernel_order = kernel_order
        self._to_sort = to_sort
        
        if self._to_sort:
            self._x_train.sort(axis = 1)

        self._buildK()
        self._buildDiffusionMap()

    def _buildK(self):
        """Internal method that builds up the kernel matrix; will be called upon initializing. Stores the relevant information in the self._K and self._lambda_x fields, doesn't return anything."""
        
        bw_percentile = self._bw_quantile*100
        n_train = self._x_train.shape[0] # find out dimension of
        dist_theta = np.power(spatial.distance.cdist(self._theta_train, self._theta_train),self._kernel_order) # distance between theta pairs
        dist_x = np.power(spatial.distance.cdist(self._x_train, self._x_train),self._kernel_order) # distance between x pairs
        lambda_theta = np.percentile(dist_theta, bw_percentile)
        lambda_x = np.percentile(dist_x, q = bw_percentile)
        
        K_theta = np.exp(-dist_theta/lambda_theta)
        K_x = np.exp(-dist_x/lambda_x)
        off_block = np.diagflat(np.repeat(self._lambda_aff, n_train))
        K_full = np.vstack([np.hstack([K_theta,off_block]),np.hstack([off_block,K_x])])

        self._K = K_full
        self._lambda_x = lambda_x

    def _buildDiffusionMap(self, maxdim = 50):
        """Internal method that builds the diffusion map. Doesn't return, just stores the relevant results in self._eta_hat, self_T_hat, self_eigs."""
        n = self._K.shape[0]
        v = np.sqrt(self._K.sum(axis = 1))
        A = self._K / (v * v.transpose())
        neff = min(maxdim + 1, n)
        decomp_vals, decomp_vecs = LA.eig(A)
        
        tmp_indices = np.absolute(decomp_vals).argsort()[::-1]
        eigenvals = decomp_vals[tmp_indices][range(neff)]
        eigenvecs = (decomp_vecs.transpose()[tmp_indices,]).transpose()
        psi = eigenvecs[:,range(neff)] / (np.matrix(eigenvecs[:,0]).transpose() * np.matrix(np.ones(neff)))
        phi = np.multiply(eigenvecs[:,range(neff)],np.matrix(eigenvecs[:,0]).transpose() * np.matrix(np.ones(neff)))
        
        lambda_vec = eigenvals[range(1,neff)] / (1 - eigenvals[range(1,neff)])
        lambda_mat = np.matrix(np.ones(n)).transpose() * np.matrix(lambda_vec)
        
        lam = lambda_mat[0,] / lambda_mat[0,0]
        neigen = min((lam > 0.05).sum() + 1, maxdim)
        eigenvals = eigenvals[np.arange(1,neigen+1)]
        X = np.multiply(psi[:,range(1,(neigen+1))],lambda_mat[:,range(neigen)])

        self._eta_hat = X[range(n/2),]
        self._T_hat = X[range(n/2,n),]
        self._eigs = eigenvals

    def nystrom(self, x_new, dims_keep):
        """Object method that projects new data into the common space via the Nystrom extension.
        
        Keyword arguments:
        x_new -- new data to project into the common space
        dims_keep -- which dimensions of the common space mapping to retain
        """
        
        T_dim = self._T_hat.shape[1] # columns of T
        eigs_divide = np.diag(self._eigs**(-1.0))
        
        if self._to_sort:
            x_new.sort(axis = 1)
        
        Dnew = spatial.distance.cdist(x_new, self._x_train) # distance between theta pairs
        Nnew = Dnew.shape[0]
        Nold = Dnew.shape[1]

        Knew = np.exp(-np.power(Dnew,self._kernel_order)/self._lambda_x)
        v = Knew.sum(axis = 1)
        tmp_denom = (np.matrix(v).transpose() * np.ones(Nold))
        weights = Knew / tmp_denom
        Xnew = weights * self._T_hat
        Xnew_rescale = Xnew * eigs_divide
        
        return_value = Xnew_rescale[:,dims_keep]
        return(return_value)
    
    def approx_suff(self, dims_keep, pairwise_mi = False):
        """Object method that computes either of two metrics related to mutual information that aim to quantify approximate sufficiency
            
        Keyword arguments:
        dims_keep -- which dimensions of the common space mapping to retain in calculating the MI metrics
        pairwise_mi -- bool, do we compute the so-called pairwise MI (True) or direct (False, the default)
        """
        theta_mat = self._theta_train
        T_mat = self._T_hat[:,dims_keep]
        
        if theta_mat.ndim == 1:
            theta_mat = np.reshape(theta_mat, (theta_mat.shape[0],1))
        if T_mat.ndim == 1:
            T_mat = np.reshape(T_mat, (T_mat.shape[0],1))
        
        if not pairwise_mi:
            theta_list = np.ndarray.tolist(theta_mat)
            T_list = np.ndarray.tolist(T_mat)
            direct_mi = ee.mi(x = theta_list, y = T_list)
            return direct_mi

        # theta_distance_list = np.ndarray.tolist(np.reshape(spatial.distance.pdist(theta_mat), (num_combs,1)))
        # T_distance_list = np.ndarray.tolist(np.reshape(spatial.distance.pdist(T_mat), (num_combs,1)))
        pair_indices = np.reshape(np.arange(theta_mat.shape[0]),[theta_mat.shape[0]/2,2])
        # Willing to believe there's a more clever way to do this...
        # compute all pairwise distances
        theta_dist_mat = spatial.distance.squareform(spatial.distance.pdist(theta_mat))
        T_dist_mat = spatial.distance.squareform(spatial.distance.pdist(T_mat))
        # first initialize the empty arrays
        theta_dist_pair = np.zeros(theta_mat.shape[0]/2)
        T_dist_pair = np.zeros(theta_mat.shape[0]/2)
        # then pick out the ones relevant to the pairs
        for [ii,jj] in pair_indices:
            theta_dist_pair[ii/2] = theta_dist_mat[ii,jj]
            T_dist_pair[ii/2] = T_dist_mat[ii,jj]

        theta_distance_list = np.ndarray.tolist(np.reshape(theta_dist_pair, [theta_mat.shape[0]/2,1]))
        T_distance_list = np.ndarray.tolist(np.reshape(T_dist_pair, [theta_mat.shape[0]/2,1]))
        mi_pairwise = ee.mi(x = theta_distance_list, y = T_distance_list)
        return(mi_pairwise)

    # Public method that generates plots of the mapping
    def plot_csmm(self, color_arg, file_arg, dims_arg = range(2)):
        """Object method that saves a diagnostic visualization of the common space mapping, showing where it maps parameters (squares) and data (triangles).
            
        Keyword arguments:
        dims_arg -- which dimensions of the mapping to plot (default: the first two)
        color_arg -- how to color the parameter-data pairs
        file_arg -- file name where we'll save the plot
        """
        n_build = self._T_hat.shape[0]
        cmap_use = plt.cm.get_cmap('Spectral')
        x_dim = dims_arg[0]
        y_dim = dims_arg[1]
        
        s_use = 100
        plt.scatter(self._T_hat[:,x_dim], self._T_hat[:,y_dim], marker = '^', c = color_arg, cmap = cmap_use, s = s_use)
        plt.scatter(self._eta_hat[:,x_dim], self._eta_hat[:,y_dim],marker = 's', c = color_arg, cmap = cmap_use, s = s_use)
        
        for point_iter in range(self._T_hat.shape[0]):
            plt.plot([self._T_hat[point_iter,x_dim],self._eta_hat[point_iter,x_dim]],[self._T_hat[point_iter,y_dim],self._eta_hat[point_iter,y_dim]], color = 'dimgrey', alpha = 0.5)

        plt.colorbar()
        plt.savefig(file_arg)
        plt.clf()