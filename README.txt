This repository contains some code related to my research on constructing 
approximately sufficient statistics for approximate Bayesian computation.

A brief description of the contents:
- csmm.py: Defines the csmm class which learns the common-space mapping method, 
our approach to constructing approximately sufficient statistics for use as
summary statistics in ABC. Other methods are present for computing diagnostics
and plotting the mapping. The diagnostics depend on the mutual information
estimator implemented in Greg Ver Steeg's NPEET toolbox, available here:
http://www.isi.edu/~gregv/npeet.tgz. It should suffice to copy the 
entropy_estimators.py file into the same directory as csmm.py.

- abc_smc.py: Contains a Python implementation of the ABC SMC algorithm (see, 
e.g., Toni et al. (2009)) for iteratively resampling and perturbing particles 
(with a successively shrinking tolerance \epsilon), which is a more efficient 
approach than the simple rejection ABC algorithm. 

- unknown_tau: Code you need to obtain ABC SMC approximation to the posterior
distribution of the normal precision (\tau) in the setting where \mu is known
and \tau is drawn from a Gamma prior. These simulations compare the results of 
ABC SMC posterior estimation when summarizing via four summary statistics: the 
known minimal sufficient statistic, the CSMM-derived statistic, the IQR, and the 
mean. Note that this exercise is purely for pedantic purposes; there is no reason 
to do ABC in such a simple situation where the posterior is known analytically.