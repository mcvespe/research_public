import numpy as np
from scipy import spatial, stats
from datetime import datetime

def abc_smc(summary_func, obs_data, prior_object, model_draw_func, num_particles, time_steps, perturbation_bw_start, epsilon_start):
    """Implements the ABC-SMC algorithm of Toni et al. (2009), iteratively resampling and perturbing particles (with successively decreasing tolerance) to obtain an ABC sample. Returns a dictionary of 'particles' and 'weights'.
        
        Keyword arguments:
        # summary_func -- a function that maps data to summary statistics
        # obs_data -- the observed data
        # prior_object -- a scipy.stats RV object (with rvs and pdf methods) for the prior
        # model_draw_func -- function that takes one set of parameter values and returns one draw from the model
        # num_particles -- number of particles
        # time_steps -- how many times do we iteratively resample/perturb/accept?
        # perturbation_bw_start -- initial bandwidth for the Gaussian perturbation
        # epsilon_start -- tolerance value to use in first iteration
        """
    project_obs = summary_func(obs_data)
    
    epsilon = epsilon_start
    theta_particles = np.matrix(prior_object.rvs(num_particles)).T # initialize particles with prior draws
    particle_weights = np.ones(num_particles)/num_particles # initialize weights with uniform
    particle_distances = np.zeros(num_particles)
    
    theta_particles_new = theta_particles.copy()
    particle_weights_new = particle_weights.copy()
    
    for time_iter in range(time_steps):
        print(" time: " + datetime.now().strftime('%H:%M:%S') + " step: " + str(time_iter) + " epsilon: " + str(epsilon))
        perturbation_bw_current = perturbation_bw_start*(0.9**time_iter)
        for particle_iter in range(num_particles):
            KEEP_TRYING = True # keep going until we have accepted a particle
            while (KEEP_TRYING):
                # sample (weightedly) one of the particles from the previous iteration
                candidate_index = np.random.choice(a = range(num_particles), size = 1, p = particle_weights)[0]
                perturbation_add = np.random.normal(loc = 0,scale = perturbation_bw_current,size = theta_particles.shape[1])
                candidate_theta = np.add(theta_particles[candidate_index,],perturbation_add)
                # check to see that the candidate parameter value is contained in the prior support
                if prior_object.pdf(candidate_theta) > 0:
                    candidate_x = model_draw_func(candidate_theta)
                    project_cand = summary_func(candidate_x)
                    dist_cand_obs = spatial.distance.cdist(np.matrix(project_cand), np.matrix(project_obs))
                    if dist_cand_obs < epsilon:
                        theta_particles_new[particle_iter,] = candidate_theta # accept the particle
                        particle_distances[particle_iter] = dist_cand_obs # store the distance
                        particle_dist_ij = spatial.distance.cdist(np.matrix(candidate_theta),np.matrix(theta_particles))
                        evaluate_kernels = stats.norm.pdf(particle_dist_ij,loc = 0,scale = perturbation_bw_current)[0]
                        particle_weights_new[particle_iter] = prior_object.pdf(candidate_theta)/sum(np.multiply(evaluate_kernels, particle_weights))
                        KEEP_TRYING = False
        
        # Having accepted all the new particles for this iteration...
        theta_particles = theta_particles_new.copy()
        particle_weights = particle_weights_new.copy() / np.sum(particle_weights_new)
        # ...use as the next tolerance the median of this iteration's distances
        epsilon = np.percentile(particle_distances, 50)

    return({'particles':theta_particles, 'weights': particle_weights})